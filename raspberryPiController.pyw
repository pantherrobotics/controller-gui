import pygame, math

pygame.init()
window = pygame.display.set_mode((1000,800))
pygame.display.set_caption("Controller Software")

done = False

class Vertex:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def to2D(self, cam):
        f = (self.z - cam.z) / 400
        vx = self.x / f + 500
        vy = self.y / f + 400
        return [vx, vy]

class Edge:
    def __init__(self, v1, v2, color):
        self.vecticies = [v1, v2]
        self.color = color

class Surface:
    def __init__(self, verticies, color):
        self.verticies = verticies
        self.color = color

class Cube:
    def __init__(self, pos):
        self.pos = pos
        self.verticies = [
            Vertex(-50, -50, -50),
            Vertex(-50, 50, -50),
            Vertex(50, 50, -50),
            Vertex(50, -50, -50),
            Vertex(-50, -50, 50),
            Vertex(-50, 50, 50),
            Vertex(50, 50, 50),
            Vertex(50,- 50, 50)
        ]
        self.edges = [
            Edge(0, 1, (0, 255, 255)),
            Edge(1, 2, (0, 255, 255)),
            Edge(2, 3, (0, 255, 255)),
            Edge(3, 0, (0, 255, 255)),
            Edge(4, 5, (0, 255, 255)),
            Edge(5, 6, (0, 255, 255)),
            Edge(6, 7, (0, 255, 255)),
            Edge(7, 4, (0, 255, 255)),
            Edge(0, 4, (0, 255, 255)),
            Edge(1, 5, (0, 255, 255)),
            Edge(2, 6, (0, 255, 255)),
            Edge(3, 7, (0, 255, 255))
        ]
        self.surfaces = [
            Surface([0, 1, 2, 3], (255, 0, 0)),
            Surface([4, 5, 6, 7], (0, 255, 0)),
            Surface([0, 1, 5, 4], (0, 0, 255)),
            Surface([2, 3, 7, 6], (255, 255, 0)),
            Surface([2, 3, 4, 5], (0, 255, 255)),
            Surface([0, 1, 7, 6], (255, 0, 255))
        ]

def rotate_point(cx, cy, angle, px, py):
    s = math.sin(angle)
    c = math.cos(angle)
    px -= cx
    py -= cy
    xnew = px * c - py * s
    ynew = px * s + py * c
    px = xnew + cx
    py = ynew + cy
    return [px, py]

def bubbleSort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1] :
                arr[j], arr[j+1] = arr[j+1], arr[j]

camera = Vertex(0, 0, -200)
cube = Cube([0, 0, 0])

while not done:
    pygame.draw.rect(window, (0, 0, 0), [0, 0, 1000, 800])
    for v in cube.verticies:
        vpos = v.to2D(camera)
        pygame.draw.circle(window, (0, 255, 255), vpos, 3)
    for e in cube.edges:
        vpos1 = cube.verticies[e.vecticies[0]].to2D(camera)
        vpos2 = cube.verticies[e.vecticies[1]].to2D(camera)
        pygame.draw.line(window, e.color, vpos1, vpos2, width=5)
    for s in cube.surfaces:
        verts = []
        for v in s.verticies:
            verts.append(cube.verticies[v].to2D(camera))
        pygame.draw.polygon(window, s.color, verts)
    for v in cube.verticies:
        tempXY = rotate_point(0, 0, 0.001, v.x, v.y)
        tempXZ = rotate_point(0, 0, 0.001, tempXY[0], v.z)
        v.x = tempXZ[0]
        v.y = tempXY[1]
        v.z = tempXZ[1]
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            done = True
    pygame.display.update()